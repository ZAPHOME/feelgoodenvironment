# Configuration
BASE_PROJECT="bitbucket.org/zaphome"
ADAPTERS="dev mili rest web"
SKIP_TESTS=1
SKIP_CORE=0

# Process parameters
usage() {
	echo Zaphome Build System
	echo "-a | --adapters	Build only these adapters (-a \"dev rest\")"
	echo "-h | --help	Display this help text"
	echo "--skipTests	Skip the execution of tests"
	echo "--skipCore	Skip the build pipeline for the core projects (sharedapi, central, adapterbase)."
	echo "          	This will also deactivate the cleaning"
}

while [ "$1" != "" ]; do
    case $1 in
        -a | --adapters )       shift
                                ADAPTERS=$1
                                ;;
        --skipTests )           SKIP_TESTS=1
                                ;;
        --skipCore )            SKIP_CORE=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

# Clean
if [ "$SKIP_CORE" = "0" ]; then
	echo "\033[1;34m---- Clean ----\033[0;39m"
	rm -dr bin pkg
	mkdir bin pkg
	echo Finished
	echo
fi

# Pipeline Definition
pipeline() {
	PROJECT_PATH=$BASE_PROJECT/$1/$2
	echo "\033[1;34m---- Pipeline $1 ----\033[0;39m"

	# Build interceptor
	if [ -e src/$BASE_PROJECT/$1/build-interceptor.sh ]; then
		echo "\033[0;34m-- BUILD INTERCEPTOR --\033[0;39m"
		cd src/$BASE_PROJECT/$1
		chmod a+x build-interceptor.sh
		./build-interceptor.sh
		cd $GOPATH
		echo Finished
		echo
	fi

	# Build
	echo "\033[0;34m-- BUILD --\033[0;39m"
	go build $3 $PROJECT_PATH
	echo Finished
	echo

	# Test
	echo "\033[0;34m-- TEST --\033[0;39m"
	if [ "$SKIP_TESTS" = "0" ]; then
		go test $PROJECT_PATH
	else
		echo Skipped
	fi
	echo
}

# Project specific parameter
if [ "$SKIP_CORE" = "0" ]; then
	pipeline sharedapi ./... "-gcflags="""-N -l""""
	pipeline central . "-o bin/central -gcflags="""-N -l""""
	pipeline adapterbase ./... "-gcflags="""-N -l""""
fi

for ADAPTER in $ADAPTERS; do
	PROJECT_NAME=$ADAPTER"adapter"
	OUTPUT_PATH="bin/adapter/"$PROJECT_NAME".so"
  pipeline $PROJECT_NAME . "-buildmode=plugin -o "$OUTPUT_PATH
done
