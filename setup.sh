# Configuration
BASE_PROJECT="bitbucket.org/zaphome"
ADAPTERS="dev mili rest web"
UPDATE="" # "": setup mode -- "-u": update mode

# Process parameters
usage() {
	echo Zaphome Setup Build System
	echo "-a | --adapters	Setup only these adapters (-a \"dev rest\")"
	echo "-h | --help	Display this help text"
	echo "-u | --update	Updates the working copy"
}

while [ "$1" != "" ]; do
    case $1 in
        -a | --adapters )       shift
                                ADAPTERS=$1
                                ;;
        -u | --update )         UPDATE="-u"
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

# Get the current project
go get $UPDATE $BASE_PROJECT/sharedapi/./...
go get $UPDATE $BASE_PROJECT/adapterbase/./...
go get $UPDATE $BASE_PROJECT/central/./...

for ADAPTER in $ADAPTERS; do
  go get $UPDATE $BASE_PROJECT/$ADAPTER"adapter"/./...
done
