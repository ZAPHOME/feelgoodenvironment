FROM golang:1.9-stretch
WORKDIR $GOPATH

# Build and run
ENTRYPOINT rm -drf bin pkg &&\
           go get github.com/cespare/reflex &&\
           ./bin/reflex -r "\.go$" -s -- sh -c "cd /go && ./build.sh $BUILD_ARGS && cd bin && ./central"
