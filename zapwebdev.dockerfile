FROM node:alpine
RUN mkdir /webapp
WORKDIR /webapp

# Serve webapp in development mode
EXPOSE 4200
ENTRYPOINT npm i && npm start
